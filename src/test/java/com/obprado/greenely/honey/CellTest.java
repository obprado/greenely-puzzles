package com.obprado.greenely.honey;

import java.util.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class CellTest {    
    private Cell cell;
    private Collection<Cell> adjacentCells;
    private Cell addedCell;

    @Before
    public void setUp() {
        adjacentCells = new ArrayList<>();
    }

    @Test
    public void shouldKnowWhenTwoCellsAreEqual() {
        given(theOrigin());
        thenItShouldBeEqualTo(theOrigin());
    }

    @Test
    public void shouldKnowWhenTwoCellsAreDifferent() {
        given(theOrigin());
        thenItShouldNotBeEqualTo(aCell(1, 0));
    }

    @Test
    public void shouldDetectWhenItIsComparedWithANonCellObject() {
        given(theOrigin());
        thenItShouldNotBeEqualTo(new Object());
    }
    
    @Test
    public void shouldAddCoordinates(){        
        given(theOrigin());
        whenCalculatedAdding(1, 0);
        thenTheAddedCellShouldBe(aCell(1, 0));
    }
    
    @Test
    public void shouldAddTheOrigin(){
        given(aCell(-1, -1));
        whenCalculatedAdding(0, 0);
        thenTheAddedCellShouldBe(aCell(-1, -1));
    }

    @Test
    public void shouldFindAdjacentCellsForTheOrigin() {
        given(theOrigin());
        whenAskedForTheAdjacentCells();
        thenTheAdjacentCellsShouldBe(firstRingOfCells());
    }
    
    @Test
    public void shouldFindAdjacentCells() {
        given(aCell(1, 0));
        whenAskedForTheAdjacentCells();
        thenTheAdjacentCellsShouldBe(adjacentTo_1_0());
    }

    private Cell aCell(int a, int b) {
        return new Cell(a, b);
    }

    private void given(Cell aCell) {
        cell = aCell;
    }

    private void whenAskedForTheAdjacentCells() {
        adjacentCells = cell.adjacentCells();
    }

    private Collection<Cell> firstRingOfCells() {
        return Arrays.asList(
                aCell(1, 0),
                aCell(0, 1),
                aCell(-1, 1),
                aCell(-1, 0),
                aCell(0, -1),
                aCell(1, -1)
        );
    }

    private void thenTheAdjacentCellsShouldBe(Collection<Cell> expectedCells) {
        for (Cell expectedCell : expectedCells) {
            assertTrue("The expected cell should be considered adjacent",
                    adjacentCells.contains(expectedCell));
        }
    }

    private void thenItShouldBeEqualTo(Cell expected) {
        assertTrue("The cells should know they are equal", cell.equals(expected));
    }

    private void thenItShouldNotBeEqualTo(Object expected) {
        assertFalse("The cells should know they are different", cell.equals(expected));
    }

    private Cell theOrigin() {
        return aCell(0, 0);
    }

    private Collection<Cell> adjacentTo_1_0() {
        return Arrays.asList(
                aCell(2, 0),
                aCell(1, 1),
                aCell(0, 1),
                theOrigin(),
                aCell(1, -1),
                aCell(2, -1)
        );
    }

    private void whenCalculatedAdding(int coordinateA, int coordinateB) {
        addedCell = cell.plus(coordinateA, coordinateB);
    }

    private void thenTheAddedCellShouldBe(Cell expected) {
        assertEquals("The cell should add correctly", expected, addedCell);
    }

}

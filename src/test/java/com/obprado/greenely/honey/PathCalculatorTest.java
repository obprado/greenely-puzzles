package com.obprado.greenely.honey;

import static com.obprado.greenely.honey.PathCalculator.BAD_ARGUMENT;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author omar
 */
public class PathCalculatorTest {
    private PathCalculator calculator;
    private int calculatedPaths;
    
    @Test
    public void shouldNotBeAskedToCalculateNegativeSteps(){
        given(aCalculator());
        thenItShouldBreakWhenAskedFor(-1);
    }
    
    @Test
    public void shouldFindTheNumberOfPathsForZeroSteps(){
        given(aCalculator());
        whenItCalculatesPathsFor(zeroSteps());
        thenItShouldHave(zeroPaths());
    }    
    
    @Test
    public void shouldFindTheNumberOfPathsForOneStep(){
        given(aCalculator());
        whenItCalculatesPathsFor(oneStep());
        thenItShouldHave(zeroPaths());
    }
    
    @Test
    public void shouldFindTheNumberOfPathsForTwoSteps(){
        given(aCalculator());
        whenItCalculatesPathsFor(twoSteps());
        thenItShouldHave(sixPaths());
    }
    
    @Test
    public void shouldFindTheNumberOfPathsForThreeSteps(){
        given(aCalculator());
        whenItCalculatesPathsFor(threeSteps());
        thenItShouldHave(twelvePaths());
    }
    
    @Test
    public void shouldFindTheNumberOfPathsForFourSteps(){
        given(aCalculator());
        whenItCalculatesPathsFor(fourSteps());
        thenItShouldHave(ninetyPaths());
    }
    
    @Test
    public void shouldReturnPlusOneWhenThePathIsOverInTheOrigin(){
        given(aCalculator());
        whenItCalculatesPathsFor(theOrigin(), after(oneStep()), 
                andTheExpectedLenghtIs(oneStep()));
        thenItShouldHave(onePath());
    }
    
    @Test
    public void shouldReturnPlusZeroWhenThePathIsOverOutOfTheOrigin(){
        given(aCalculator());
        whenItCalculatesPathsFor(someCell(), after(oneStep()), 
                andTheExpectedLenghtIs(oneStep()));
        thenItShouldHave(zeroPaths());
    }
    
    private void given(PathCalculator calculator) { this.calculator = calculator; }
    private PathCalculator aCalculator() { return new PathCalculator(); }    
    private int zeroSteps() { return 0; }
    private int oneStep() { return 1; }
    private int twoSteps() { return 2; }
    private int threeSteps() { return 3; }
    private int fourSteps() { return 4; }
    private int zeroPaths() { return 0; }
    private int onePath() { return 1; }
    private int sixPaths() { return 6; }
    private int twelvePaths() { return 12; }
    private int ninetyPaths() { return 90; }
    private int after(int var) { return var; }
    private int andTheExpectedLenghtIs(int var) { return var; }
    private Cell theOrigin() { return new Cell(0,0); }
    private Cell someCell() { return new Cell(1,0); }

    private void whenItCalculatesPathsFor(int numberOfSteps) {
        calculatedPaths = calculator.pathsFor(numberOfSteps);
    }
    
    private void whenItCalculatesPathsFor(Cell currentCell, int stepsPerformed, 
            int maximumSteps) {        
        calculatedPaths = calculator.
                calculatePaths(currentCell, stepsPerformed, maximumSteps);
    }

    private void thenItShouldHave(int expectedPaths) {        
        assertEquals("The calculator should have found the expected number of paths", 
                expectedPaths, calculatedPaths);
    }

    private void thenItShouldBreakWhenAskedFor(int invalidNumberOfPaths) {
        try {
            calculator.pathsFor(invalidNumberOfPaths);
            fail("Calculator should throw an exception when invalid input is used");
        } catch (IllegalArgumentException exception){
            assertEquals("The exception should inform about the reason to invalidate the input", 
                    BAD_ARGUMENT, exception.getMessage());
        }            
    }
}
package com.obprado.greenely.honey;

import java.io.*;

/**
 *
 * @author omar
 */
public class Runner {
    
    public static void main(String [ ] args) throws IOException{
        BufferedReader inputStream = new BufferedReader(new InputStreamReader(System.in));
        PrintStream outputStream = System.out;
        int numberOfTests = Integer.valueOf(inputStream.readLine());
        for (int i = 1; i <= numberOfTests; i++){
            int input = Integer.valueOf(inputStream.readLine());
            outputStream.println(new PathCalculator().pathsFor(input));
        }            
    }   
    
}

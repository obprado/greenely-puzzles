package com.obprado.greenely.honey;

import java.util.*;

/**
 *
 * @author omar
 */
public class Cell {
    private final int CoordinateA;
    private final int CoordinateB;

    public Cell(int CoordinateA, int CoordinateB) {
        this.CoordinateA = CoordinateA;
        this.CoordinateB = CoordinateB;
    }

    @Override
    public boolean equals(Object obj) {
        if (! (obj instanceof Cell))
            return false;
        Cell other = (Cell)obj;
        return this.CoordinateA == other.CoordinateA && 
                this.CoordinateB == other.CoordinateB;
    }

    Collection<Cell> adjacentCells() { 
        Collection<Cell> valuesToAdd = valuesToAd();        
        Collection<Cell> adjacentCells = new ArrayList<>();
        for (Cell toAdd : valuesToAdd)
            adjacentCells.add(this.plus(toAdd.CoordinateA, toAdd.CoordinateB));
        return adjacentCells;
    }
    
    private Collection<Cell> valuesToAd() {
        return Arrays.asList(
                new Cell(1, 0),
                new Cell(0, 1),
                new Cell(-1, 1),
                new Cell(-1, 0),
                new Cell(0, -1),
                new Cell(1, -1)
        );
    }
    
    Cell plus(int coordinateA, int coordinateB){
        return new Cell(CoordinateA + coordinateA, CoordinateB + coordinateB);
    }

}

package com.obprado.greenely.honey;

/**
 *
 * @author omar
 */
public class PathCalculator {
    public static final Cell ORIGIN = new Cell(0,0);
    public static final String BAD_ARGUMENT = "The number of steps can't be negative";

    public int pathsFor(int numberOfSteps) {
        if (numberOfSteps < 0)
            throw new IllegalArgumentException(BAD_ARGUMENT);
        if (numberOfSteps == 0)
            return 0;
        return calculatePaths(ORIGIN, 0, numberOfSteps);
    }
    
    int calculatePaths(Cell current, int stepsPerformed, int maximumSteps){
        if (stepsPerformed == maximumSteps)            
            return baseCase(current);
        
        int acumulatedPaths = 0;
        for (Cell nextCell : current.adjacentCells())
            acumulatedPaths += calculatePaths(nextCell, stepsPerformed + 1, maximumSteps);
        return acumulatedPaths;
    }

    private int baseCase(Cell current) {
        if (current.equals(ORIGIN))
            return 1;
        else
            return 0;
    }
    
}
